import React from 'react';

const Footer = () => {
    return (
        <div className="text-center p-10 mt-10 border-t dark:border-gray-700 border-grey-200">
            <h1>2022 Baby Google, Inc.</h1>
        </div>
    );
}

export default Footer;
