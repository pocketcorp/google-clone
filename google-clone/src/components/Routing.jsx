import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom'
import Results from './Results'

const Routing = () => {
    return (
        <div className="p-4">
            <Routes>
                {/* <Route path={['/search', '/images', '/news', '/videos']} element={<Results />}/>
                Uncaught TypeError: meta.relativePath.startsWith is not a function */}
                <Route path="/" element={<Navigate to="/search" />} />  
                <Route path='/search' element={<Results />} />
                <Route path='/images' element={<Results />} />
                <Route path='/news' element={<Results />} />
                <Route path='/videos' element={<Results />} />     
            </Routes>
        </div>
    );
}

export default Routing;
